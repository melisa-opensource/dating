export default {
  methods: {
    getIconBack () {
      return this.$q.platform.is.ios ? 'fas fa-chevron-left' : 'fas fa-arrow-left'
    },
    getIconSettings () {
      return this.$q.platform.is.ios ? 'fas fa-cog' : 'fas fa-sliders-h'
    }
  }
}
